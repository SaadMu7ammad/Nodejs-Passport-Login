const User = require('../models/users');
const bcryptjs = require('bcryptjs');
exports.getLogin = (req, res, next) => {
  res.render('login', {
    messages: 'saad is an err',
    name: req.body.name,
    email: req.body.email,
    isLoggedIn: false,
    csurfToken:req.csrfToken()
  });
};
//qAhL0kYEiCqkJE_ntWMILAm_
exports.postLogin = (req, res, next) => {
  // console.log(req.session.isLoggedIn);
  if (req.session.isLoggedIn) {
    return res.render('index', {
      name: req.session.user.name,
      email: req.session.user.email,
      csurfToken:req.csrfToken()
      // isLoggedIn: true
    });
  }

  User.findOne({ email: req.body.email })
    .then((result) => {
      if (!result) {
        return res.redirect('/login');
      }

      bcryptjs
        .compare(req.body.password, result.password)
        .then((isEqual) => {
          if (isEqual) {
            req.session.isLoggedIn = true;
            req.session.user = result;
            req.session.save((err) => {
              // console.log(err);
              res.render('index', {
                name: result.name,
                email: result.email,
                csurfToken:req.csrfToken()
                // isLoggedIn: true
              });
            });
          } else {
            console.log(isEqual);
            console.log(result.password);
            console.log(req.body.password);
            return res.redirect('/login');
          }
        })
        .catch((error) => {
          console.log(error);
          return res.redirect('/login');
        });
    })
    .catch((error) => {
      console.log(error);
      return res.redirect('/login');
    });
};

exports.postLogout = (req, res, next) => {
  req.session.destroy(() => {
    // req.session.isLoggedIn= false
    
    res.redirect('/');
  });
};
exports.getDeleteMe = (req, res, next) => {
  console.log(req.body.email);
  res.render('del', {
    email: req.params.email,
    isLoggedIn: false,
    csurfToken:req.csrfToken()
    // email:req.body.email
  });
};
exports.postDeleteMe = (req, res, next) => {
  // User.findOneAndDelete({
  //   $and: [{ email: req.body.email }, { password: req.body.password }],
  // })

  User.findOneAndDelete({ email: req.body.email })
  .then((deletedUser) => {
    // console.log(req.body.email);
    // console.log();
    // console.log(deletedUser);
    if (!deletedUser) {
      // res.send('cant del it');
      console.log('cant del it');
    } else {
        req.session.destroy();
        // req.session.isLoggedIn = false;
        res.redirect('/register');
      }
    })
    .catch((err) => {
      console.log(err);
    });
};
