const User = require('../models/users');
const bcryptjs = require('bcryptjs');
const nodemailer = require('nodemailer');
require('dotenv').config()

const crypto = require('crypto');
exports.getSignUp = (req, res, next) => {
  res.render('register', {
    isLoggedIn: false,
    csurfToken: req.csrfToken(),
  });
};
exports.postSignUp = async (req, res, next) => {
  const hashedPass = await bcryptjs.hash(req.body.password, 12);
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: req.body.email,
      pass: 'uahkmoisloefljnj',
    },
  });

  const user = new User({
    name: req.body.name,
    email: req.body.email,
    password: hashedPass,
  });
  transporter
    .sendMail({
      to: req.body.email,
      from: 'admin@gmail.com',
      subject: 'signup succedded',
      html: 'huuraayy u make an account mr \t' + req.body.name,
    })
    .catch((err) => {
      console.log(err);
    });
  user.save().then(() => {
    console.log('user added');
    res.redirect('/');
  });
};
exports.getReset = (req, res, next) => {
  res.render('reset', {
    csurfToken: req.csrfToken(),
  });
};
exports.postReset = (req, res, next) => {
  crypto.randomBytes(32, (err, buffer) => {
    if (err) {
      console.log(err);
      return res.redirect('/reset');
    }
    const token = buffer.toString('hex');
    User.findOne({ email: req.body.email })
      .then((result) => {
        if (!result) return res.redirect('/reset');
        result.tokenUsr = token;
        return result.save();
      })
      .then((result) => {
        const transporter = nodemailer.createTransport({
          service: 'gmail',
          auth: {
            user: req.body.email,
            pass: process.env.EMAIL,
          },
        });

        transporter.sendMail({
          to: req.body.email,
          from: 'admin@gmail.com',
          subject: 'Password reset',
          html: `
          <p>You requested a password reset</p>
          <p>Click this <a href="http://localhost:3000/reset/${token}">link</a> to set a new password.</p>
        `,
        });
        res.redirect('/');
      });
  });
};
exports.getNewPass = (req, res, next) => {
  // if (req.params.token) {

  // }
  User.findOne({
    tokenUsr: req.params.token,
  })
    .then((usr) => {
      res.render('new-password', {
        path: '/new-pass',
        pageTitle: 'change Password',
        
        csurfToken: req.csrfToken(),
        passwordToken: req.params.token,
        
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
exports.postNewPass = (req, res, next) => {
  // if (req.params.token) {

  // }
  User.findOne({
    tokenUsr: req.body.passwordToken,
  })
    .then(user => {
      if (!user) return res.redirect('/reset');
      bcryptjs.hash(req.body.password, 12).then(hashed => {
        
        user.password = hashed
  
        console.log(user);
        console.log('changed');
        user.tokenUsr=undefined
        user.save()
        res.redirect('/')

      })
    })

};