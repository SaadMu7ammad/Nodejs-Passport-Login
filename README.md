# Nodejs-Passport-Login

simple application for login,register,delete account ,reset password

 [Recorded Demo](https://github.com/SaadMu7ammad/Nodejs-Passport-Login/blob/master/SignLoginDeleteDemo.mkv)

using:
- mongoose
- nodemailer to send welcome message and reset the password
- bcrypt to hashing the password
- protect routes from not logged user
- csurf
- sessions
