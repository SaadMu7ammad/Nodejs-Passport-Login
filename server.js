require('dotenv').config();
const User = require('./models/users');
const MONGODB_URI = process.env.MONGODB_URI;
const session = require('express-session');
const mongoSessionDb = require('connect-mongodb-session')(session);
const store = new mongoSessionDb({
  uri: MONGODB_URI,
  collection: 'sessions',
});
const mongoose = require('mongoose');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const path = require('path');
const csurf = require('csurf');
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(
  session({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false,
    store: store,
  })
);
const csurfProtection = csurf();

app.set('view engine', 'ejs');
app.set('views', 'views');
const userLogin = require('./routes/login');
const userRegister = require('./routes/register');
// const bcrypt = require('bcrypt')
// const passport = require('passport')
// const flash = require('express-flash')
// const methodOverride = require('method-override')
app.use(csurfProtection);
app.use(userLogin);
app.use(userRegister);

app.use((req, res, next) => {
  console.log(req.session);
  if (!req.session.user) {
    return next();
  }
  User.findById(req.session.user._id)
    .then((user) => {
      req.user = user;
      console.log('u r still logged in');
      next();
    })
    .catch((err) => console.log(err));
});

mongoose.connect(MONGODB_URI).then(() => {
  app.listen(3000);
});
