const express = require('express');

const registerController = require('../controllers/register');
const loginController = require('../controllers/login');

const router = express.Router();

router.get('/register', registerController.getSignUp);
router.post('/register', registerController.postSignUp);
// router.post('/register', registerController.addUser);
router.get('/login', loginController.getLogin);

router.get('/reset',  registerController.getReset);
router.post('/reset',  registerController.postReset);
router.get('/reset/:token',  registerController.getNewPass);
router.post('/new-pass',  registerController.postNewPass);

module.exports = router;