const express = require('express');

const loginController = require('../controllers/login');
const Auth = require('../middlewares/auth');
const router = express.Router();

router.get('/',  loginController.getLogin);
router.post('/login',  loginController.postLogin);
router.post('/logout', Auth, loginController.postLogout);
router.get('/deleteme/:email', Auth, loginController.getDeleteMe);
router.post('/deleteme', Auth, loginController.postDeleteMe);

module.exports = router;
